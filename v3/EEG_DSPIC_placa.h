#include <33EP64MC202.h>

#FUSES NOWDT                   //No Watch Dog Timer
#FUSES NOJTAG                  //JTAG disabled
#FUSES CKSFSM                  //Clock Switching is enabled, fail Safe clock monitor is enabled

#device ICSP=1
#define LED      PIN_B0
//#define RESETAD PIN_B1  N�o tem mais 
#define START   PIN_B14
#define DRDY   PIN_B13
//#define CS   PIN_B6  N�o tem mais 
#define SINC   PIN_B4
#define STEP   PIN_A4
#define RESET   PIN_B5

#pin_select U1TX=PIN_B11
#pin_select U1RX=PIN_B12

#use delay(clock=140000000,crystal=20000000)
#use STANDARD_IO( B )
#use FIXED_IO( B_outputs=PIN_B14,PIN_B0,PIN_B5 )
#use FIXED_IO( A_outputs=PIN_A4)
#use rs232(UART1, baud=1500000, stream=UART_PORT1,STOP=1,BITS =8,PARITY=N)
#use spi(MASTER, SPI1, BAUD=10000000, MODE=1, BITS=8, stream=SPI_PORT1)
#use pwm(OC1,OUTPUT=PIN_B6,TIMER=1,FREQUENCY=8750000,DUTY=50)

