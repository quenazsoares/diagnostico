
#include <EEG_DSPIC_placa.h>
byte option = 255;   // byte de comunica��o
/*
   0 - 15 altera o canalAtual na sa�da DO RHA
   16 altera a frequ�ncia de amostragem para 28, 0 kHz
   17 altera a frequ�ncia de amostragem para 20, 0 kHz
   18 altera a frequ�ncia de amostragem para 14, 0 kHz
   19 altera a frequ�ncia de amostragem para 10, 0 kHz
   20 altera a frequ�ncia de amostragem para 8, 75 kHz
   21 altera a frequ�ncia de amostragem para 7, 0 kHz
   22 altera a frequ�ncia de amostragem para 5, 0 kHz
   23 altera a frequ�ncia de amostragem para 4, 0 kHz
   24 altera a frequ�ncia de amostragem para 2, 8 kHz
   25 altera a frequ�ncia de amostragem para 2, 24 kHz
   26 altera o tempo de coleta para 1 segundo
   27 altera o tempo de coleta para 5 segundo
   28 altera o tempo de coleta para 10 segundo
   29 altera o tempo de coleta para 20 segundo
   30 altera o tempo de coleta para 60 segundo
   31 altera o tempo de coleta para indefinido
   32 teste b�sico de comunica��o (pisca o LED 4 vezes)
   33 realiza envio de sequencia de teste de comunica��o
   34 realiza a coleta apenas DO canal selecionado
   35 realiza a coleta de todos os canais
   36 realiza a coleta de todos canais exceto o escolhido
   37 realiza a coleta dos canais selecionados utilizando o comando 64
   
   64 inicia configura��o de canais a serem coletados (ser�o enviados os �ndices de todos os canais a serem coletados e enviado 65 para finalizar a configura��o)
   65 finaliza a configura��o de canais a serem coletados
   254 para qualquer coleta em andamento
   255 op��o DEFAULT para n�o realizar nenhum comando
*/
const INT DELAYS[10] = {2495,3495,4995,6995,7995,9995,13995,17495,24995,31245};   //Delays para obter cada uma das op��es de frequ�ncia de amostragem
const LONG TEMPO_COLETA = 70000000;   // N�mero de clocks DO timer para alcan�ar um segundo de coleta
int canalAtual = 0;   // Canal em que o RHA est� atualmente
int delaySel = 0;   // Delay selecionado para a frequ�ncia de amostragem selecionada
int canalSel = 0;   // �ltimo canal selecionado
int segundosCol = 1;// Dura��o da coleta em segundos
int tempoColeta;   // Tempo atual da coleta
byte buffer[4];

// Interrup��o da comunica��o RS232
#INT_RDA
void  Serialreceive(VOID) {
   option = getc ();
}

// Fun��o para piscar o LED
void blink(INT16 tempoMs, int16 nRepeticoes){
   for (INT t = 0; t < nRepeticoes; t++){
      output_HIGH (LED);
      delay_ms (tempoMs);
      output_LOW (LED);
      delay_ms (tempoMs);
   }
   RETURN;
}

//Resetar o RHA para o canalAtual 0
void resetRHA(){
   WHILE ( ! input (SINC)){
      output_low (STEP);
      delay_cycles (300);
      output_high (STEP);
      delay_cycles (300);
   }
   canalAtual = 0;
}

//Atualiza a sa�da DO RHA para o pr�ximo canal
void proximoCanal(){
   canalAtual++;
   //Borda de subida no step para trocar o canal DO RHA
   output_low (STEP);
   delay_cycles (16);
   output_high (STEP);
   IF (canalAtual >= 16) // Retorna a informa��o de canal para 0, RHA retorna ao zero automaticamente
      canalAtual = 0;
}

//Executa as configura��es iniciais para a aquisi��o com o ADS
void startAcquisition(){
   output_low (START); // Para a convers�o AD
   delay_cycles (64); // Espera tempo suficiente para resetar o ADS
   resetRHA (); // Volta o RHA para o canal 0
   output_high (START);
}

// Configura��es iniciais
void setup(){
   setup_timer2 (TMR_INTERNAL|TMR_DIV_BY_1|TMR_32_BIT); // Configura��o DO timer de 32 bits (timer23), conta os segundos de coleta
   setup_timer4 (TMR_INTERNAL|TMR_DIV_BY_1); // Configura��o DO timer de 16 bits (timer4), conta o tempo entre as amostras
   enable_interrupts (INT_RDA); // Habilita as interrup��es de comunica��o RS232
   enable_interrupts (INTR_GLOBAL); // Habilita globalmente as interrup��es
   resetRHA ();
   blink (100, 5); // Pisca o LED para indicar a inicializa��o DO sistema
}

void main(){
   setup ();
   WHILE (true){ //Loop infinito
      //Op��es de 0 a 15; Altera o canal selecionado
      IF (option <= 15){
         canalSel = option;
         resetRHA ();
         FOR (byte contCh = 0; contCh < option; contCh++)
            proximoCanal ();
         option = 255;
      }
      
      //Op��es de 16 a 25; Altera a frequ�ncia de amostragem
      IF (option >= 16&&option <= 25) {
         delaySel = DELAYS[option - 16];
         option = 255;
      }
      
      //Op��es de 26 a 31; Altera o tempo de coleta
      IF (option >= 26&&option <= 31) {
         SWITCH (option){
            CASE 26:
               segundosCol = 1;
               BREAK;
            CASE 27:
               segundosCol = 5;
               BREAK;
            CASE 28:
               segundosCol = 10;
               BREAK;
            CASE 29:
               segundosCol = 20;
               BREAK;
            CASE 30:
               segundosCol = 60;
               BREAK;
            CASE 31:
               segundosCol = -1;
               blink (50, 4) ;
            BREAK;
         }
         option = 255;
      }
      
      //Op��o 32 ; Pisca o LED 4 vezes
      IF (option == 32){
         blink (100, 5) ;
         option = 255;
      }
      
      //Op��o 33 ; Envia caracteres de teste de comunica��o
      IF (option == 33){
         //Envia 255 vezes cada caracter de 0 a 255
         for (INT caracter = 0; caracter <= 255; caracter++)
            for (INT contVez = 0; contVez <= 255; contVez++)
               putc (caracter); //Envia o caracter
               option = 255;
      }
      
      //Op��o 34 e 35
      IF (option == 34||option == 35){
         blink (500, 10); // Pisca o LED para indicar a inicializa��o do sistema
         startAcquisition ();
         output_high (LED) ; // Mant�m o LED aceso durante a coleta

         IF (option == 34) // Se for coleta de 1 canal, seleciona o canal
            for (INT8 contCh = 0; contCh < canalSel; contCh++)
               proximoCanal ();

         set_timer23 (0); // Reseta o timer de 32 bits
         set_timer4 (0); // Reseta o timer de 16 bits
         tempoColeta = 0;
         WHILE (tempoColeta < segundosCol||segundosCol == - 1) { // FOR (tempoColeta = 0; tempoColeta < segundosCol; tempoColeta++) // Foi substituido pelo while
            tempoColeta++;
            WHILE (get_timer23 () < TEMPO_COLETA){ //Mant�m o loop por 1 segundo
               //Mant�m o START baixo por 6 tclk para reiniciar a convers�o
               output_low (START);
               delay_cycles (96);
               output_high (START);
               
               IF (option == 254)
                  BREAK;
               
               WHILE (get_timer4 () < delaySel){}//N�o faz nada at� passar o delay escolhido
               // O delay m�nimo � de 35us que j� engloba todos os transit�rios da convers�o AD para qualquer filtro utilizado e OSR = 32
               
               WHILE (input (DRDY)){}//Espera at� o DRDY abaixar, indicando um dado rec�m convertido
               WHILE (!input (DRDY)){}//Espera at� o DRDY abaixar, indicando um dado rec�m convertido
               
               spi_write (18); // command read data REGISTER
               buffer[0] = spi_read (0); // reading data
               buffer[1] = spi_read (0); // reading data
               buffer[2] = spi_read (0); // reading data
               
               //Encaminhar para o PC
               putc (buffer[0]);
               putc (buffer[1]);
               putc (buffer[2]);
               
               IF (option == 35)
                  proximoCanal ();
               set_timer4 (get_timer4 () - delaySel);
            }
            set_timer23 (get_timer23 () - TEMPO_COLETA);
            IF (option == 254)
               BREAK;
         }
         option = 255;
      }
   }
}

