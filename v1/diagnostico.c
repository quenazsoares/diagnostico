/*
   Software para o EEG utilizando o conversor AD ADS127L01
   Vers�o que posibilita a altera��o de diversos par�metros de coleta
   Autor: Quenaz B. Soares 05/02/2018
   
   A coleta � realizada no �ltimo momento antes de trocar de canal
*/

#include <uart_test.h>
#include <diagnostico.h>

byte com = 255;   // Byte de comunica��o do computador com o DSPIC
/* Comandos dispon�veis
0-15  Alterar canal selecionado do RHA
16    Inicia configura��o de canais selecionados
17    Termina configura��o de canais selecionados

20    Altera a frequ�ncia de amostragem do sistema para 28 kHz
21    Altera a frequ�ncia de amostragem do sistema para 20 kHz
22    Altera a frequ�ncia de amostragem do sistema para 16 kHz
23    Altera a frequ�ncia de amostragem do sistema para 14 kHz
24    Altera a frequ�ncia de amostragem do sistema para 10 kHz
25    Altera a frequ�ncia de amostragem do sistema para 8 kHz
26    Altera a frequ�ncia de amostragem do sistema para 4 kHz

40    Altera o tempo de coleta para indefinido
41    Altera o tempo de coleta para 1 segundo
42    Altera o tempo de coleta para 2 segundo
43    Altera o tempo de coleta para 5 segundo
44    Altera o tempo de coleta para 10 segundo
45    Altera o tempo de coleta para 30 segundo
46    Altera o tempo de coleta para 60 segundo
47    Altera o tempo de coleta para 120 segundo
48    Altera o tempo de coleta para 300 segundo

50    Altera o tipo de coleta para todos canais
51    Altera o tipo de coleta para canal selecionado
52    Altera o tipo de coleta para todos canais exceto o selecionado
53    Altera o tipo de coleta para canais selecionados 
54    Altera o tipo de coleta para dados simulados (rampa)
55    Altera o tipo de coleta para dados simulados (aleat�rio)
56    Altera o tipo de coleta para dados simulados (degrau, 0-255)
57    Altera o tipo de coleta para dados simulados (constante 0)

60    Inicia coleta
61    For�a parada da coleta
65    Envia sequ�ncia de teste, 256 caracteres de cada de 0 a 255
66    Teste b�sico de comunica��o, pisca LED 4 vezes
*/

// Delay para cada configura��o de frequ�ncia de amostragem do sistema
const int DELAYS[] = {2495,3495,4370, 4995, 6995, 8745, 17495};
int   delayAtual = 2495;   // Delay selecionado para atingir a frequ�ncia de amostragem desejada
int16 tempoCol = 0;        // Tempo de coleta em segundos, se 0 a coleta durar� por tempo indefinido

int   canalAtual = 0;      // Canal atualmente exibido na sa�da do RHA
int   canalSel = 0;        // Canal selecionado atrav�s da configura��o
int   canaisSel[16];       // Canais selecionados para coleta m�ltipla
int   tipoCol=0;
int8  buffer[12];
int32 simData = 0;         // Vari�vel tempor�ria para realizar a simula��o de coleta de dados

// Interrup��o de dado dispon�vel na comunica��o RS232
#INT_RDA
void Serialreceive(){
   com = getc();  // Obt�m o dado recebido
   if (com==16){  // Tratamento para a sele��o de v�rios canais
      for (int cont=0; cont<16; cont++)
         canaisSel [cont] = 0;   // Zera a sele��o dos canais
      while (com!=17){  // Para a configura��o quando receber o comando 17
         com = getc();
         if(!(com>17 || com==16))   // Verifica se o comando � v�lido para a situa��o
            canaisSel[com]=1;    // Seleciona o canal
      }
   }
}

// Fun��o respons�vel por realizar a piscada do LED
void blink(int tempoms, int repeticoes){
   for(int cont=0; cont<repeticoes; cont++){
      output_high(LED);
      delay_ms(tempoms);
      output_low(LED);
      delay_ms(tempoms);
   }
}

// Realiza as configura��es iniciais do sistema de coleta
void setup(){
   enable_interrupts(GLOBAL);  // Habilita as interrup��es globais
   enable_interrupts(INT_RDA); // Habilita as interrup��es da comunica��o RS232
   
   for(int cont=0; cont<16; cont++){
      canaisSel[cont] = 0;
   }
   // Configura os timers utilizados
   setup_timer2 (TMR_INTERNAL|TMR_DIV_BY_1|TMR_32_BIT);
   setup_timer4 (TMR_INTERNAL|TMR_DIV_BY_1);
   
   output_low(CS);
   resetCanal();
   
   blink(100,5);
}


// Reseta o RHA para o canal 0
void resetCanal(){
   output_low(RESET);
   delay_cycles(50);
   output_high(RESET);
   canalAtual = 0;
}

// Muda a sa�da do RHA para o proximo canal
void proximoCanal(){
   output_low(STEP);
   delay_cycles(10);
   output_high(STEP);
   delay_cycles(10);
   canalAtual++;
   if (canalAtual ==16)
      canalAtual=0;
}

// Muda a sa�da do RHA para o canal escolhido
void setCanal(int canal){
   resetCanal();
   while(canalAtual!=canal)
      proximoCanal();
}

// Realiza o teste de comuica��o, enviando 256 vezes cada caracter de 0 a 255
void testeComunicacao(){
   for(int caracter=0; caracter<256; caracter++)
      for(int cont=0; cont<256; cont++)
         putc(caracter);
}

void main(){
   setup();
   while(true){
      // Altera o canal selecionado
      if(com<16){
         canalSel = com;
         setCanal(com);
         com=255;
      }

      // Configura a frequ�ncia de amostragem do sistema
      if((com>=20) && (com<=26)){
         delayAtual = DELAYS[com-20];
         com=255;
      }
      
      // Configura o tempo de coleta
      if(com>=40 && com<49){
         int16 tempos[]={0,1,2,5,10,30,60,120,300};
         tempoCol=tempos[com-40];
         com=255;
      }
      
      // Configura o tipo de coleta
      if(com>=50 && com<58){
         tipoCol = com;
         com=255;
      }
      
      if(com==65){
         testeComunicacao();
         com=255;
      }
      if(com==66){
         blink(125,4);
         com=255;
      }
         
      // Inicia coleta
      if(com==60){
         output_low(START);      // Para a convers�o AD
         output_low(CS);         // Inicia a comunica��o SPI
         output_low(RESETAD);   // Reseta a convers�o AD
         delay_cycles(64);      // Espera tempo suficiente para resetar o ADS
         output_high(RESETAD);     
         output_high(START);
         
         // Altera o canal para o inicial de acordo com o tipo de coleta escolhido
         resetCanal();
         if(tipoCol==51)
            setCanal(canalSel);
         if(tipoCol==52)
            if(canalAtual==canalSel)
               proximoCanal();
         if(tipoCol==53)
            while(canaisSel[canalAtual]==0)
               proximoCanal();
         
         blink(100,2); // Indica in�cio da coleta
         
         //Reseta os timers para controle de tempo
          set_timer23(0);
          set_timer4(0);
          unsigned int repeticoes = 0;
          
          while((repeticoes<tempoCol) || (tempoCol==0)){  // Repete enquanto n�o atingir o tempo desejado ou indefinidamente se tempoCol=0
            repeticoes++;
            if(com==61)
               break;
            while(get_timer23()<TEMPOCOLETA){         // Loop para manter a coleta por 1 segundo
               //Mant�m o START baixo por 4 tclk para reiniciar a convers�o
               output_low(START);
               delay_cycles(64);
               output_high(START);
               
               if (com==61)
                  break;
               
               while(get_timer4()<tempoCol){}         // N�o faz nada at� passar o delay escolhido
               // O delay m�nimo � de 35us que j� engloba todos os transit�rios da convers�o AD para qualquer filtro utilizado e OSR = 32
               
               if (tipoCol>=50 && tipoCol<53){        // Coletar dados reais
                  while(input(DRDY)){}//Espera at� o DRDY abaixar, indicando um dado rec�m convertido
                  // Ler dados do ADS127L01
                  spi_write (18);    // command read data REGISTER
                  buffer[0] = spi_read (0);  // reading data
                  buffer[1] = spi_read (0);  // reading data
                  buffer[2] = spi_read (0);  // reading data
                  
                  //Enviar dados para o computador
                  putc(buffer[0]);
                  putc(buffer[1]);
                  putc(buffer[2]);
                  
                  if(tipoCol!=51){              // Se o tipo de coleta for de apenas um canal trocado o canal da sa�da do RHA
                     if(tipoCol==50){           // Coleta de todos canais
                        proximoCanal();
                     }else if(tipoCol==52){     // Coleta de todos canais exceto o selecionado
                        proximoCanal();
                        if(canalAtual==canalSel)
                           proximoCanal();
                     } else if(tipoCol==53)     // Coleta dos canais selecionados
                        do{
                           proximoCanal();
                        }while(canaisSel[canalAtual]==0);
                  }
                  
               }else{   // Tipos de coleta simuladas
                  if(tipoCol == 54){
                     simData++;
                     if(simData>MAXSIM)
                        simData = MINSIM;
                  }else if(tipoCol == 55)
                     simData = 0; //rand();
                  else if(tipoCol ==56){
                     if(simData == MAXSIM)
                        simData=MINSIM;
                     else
                        simData=MAXSIM;
                  }else
                     simData = MINSIM;
                  
                  putc(make8(simData,0));
                  putc(make8(simData,0));
                  putc(make8(simData,0));
               }
               set_timer4(get_timer4()-delayAtual);
            }
            set_timer23(get_timer23()-TEMPOCOLETA);
          }
         com = 255;
      }
   }
}

