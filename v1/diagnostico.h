//Fun��es
// Fun��o respons�vel por realizar a piscada do LED
void blink(int tempoms, int repeticoes);

// Realiza as configura��es iniciais do sistema de coleta
void setup();

// Reseta o RHA para o canal 0
void resetCanal();

// Muda a sa�da do RHA para o proximo canal
void proximoCanal();

// Muda a sa�da do RHA para o canal escolhido
void setCanal(int canal);

// Realiza o teste de comuica��o, enviando 256 vezes cada caracter de 0 a 255
void testeComunicacao();

// Constantes
#define TEMPOCOLETA  70000000   // N�mero de clocks do timer para alcan�ar um segundo de coleta

#define DELAY28K     2495 // Delay necess�rio para se obter uma frequencia de amostragem de 28kHz
#define DELAY20K     3495 // Delay necess�rio para se obter uma frequencia de amostragem de 20kHz
#define DELAY16K     4370 // Delay necess�rio para se obter uma frequencia de amostragem de 16kHz
#define DELAY14K     4995 // Delay necess�rio para se obter uma frequencia de amostragem de 14kHz
#define DELAY10K     6995 // Delay necess�rio para se obter uma frequencia de amostragem de 10kHz
#define DELAY8K      8745 // Delay necess�rio para se obter uma frequencia de amostragem de 8kHz
#define DELAY4K     17495 // Delay necess�rio para se obter uma frequencia de amostragem de 4kHz

// Constantes de configura��o da taxa de amostragem do MAX11040
#define WRITEDATARATE 0b01010000
#define DATARATE64K   0b11100000
#define DATARATE32K   0b11000000
#define DATARATE16K   0b00000000
#define DATARATE08K   0b10100000
#define DATARATEFINE  0b00000000

// Constantes de configura��o do MAX11040
#define WRITECONFIG   0b01100000
#define DEFAULTCONF   0b00110000 // XTALEN=1 e EN24BIT=1
#define READDATA      0b11110000

#define MINSIM        0
#define MAXSIM        8388607    // Maior n�mero representado com 23 bits, para evitar complemento de 2
