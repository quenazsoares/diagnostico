function [ dadosCol, controle ] = coleta( fs, dur, tipo, porta )
%COLETA Realiza a coleta com o ADS127
%   fs   - Frequencia de amostragem
%   dur  - Dura��o da coleta em segundos
%   tipo - Tipo de coleta
%           0 - Todos canais em sequencia
%           1 - Todos canais individualmente
%   porta- Porta de comunica��o em uso

nAmostra = fs*dur;
dadosB = zeros(1,nAmostra*3);  % Prealoca a matriz para armazenar os dados brutos
contT = 0;	% Contador para o tempo de coleta

% Obt�m o comando para configurar a frequencia de amostragem
fsCom=16;
if fs==28000
    fsCom=16;
elseif fs==20000
    fsCom=17;
elseif fs==14000
    fsCom=18;
elseif fs==10000
    fsCom=19;
elseif fs==8750
    fsCom=20;
elseif fs==7000
    fsCom=21;
elseif fs==5000
    fsCom=22;
elseif fs==5000
    fsCom=22;
elseif fs==4000
    fsCom=23;
end

try     % Tenta fechar todos as comunica��es
    fclose(instrfindall);
    delete(instrfindall);
catch
end

global dsPIC
dsPIC = serial(porta,'baud', 1500000,'InputBufferSize',fs*16,...
    'BytesAvailableFcn',@callback,'BytesAvailableFcnCount',fs*4,...
    'BytesAvailableFcnMode','byte');
fopen(dsPIC);

fwrite(dsPIC,254);       % Para qualquer coleta que esteja em andamento
pause(0.01)
fwrite(dsPIC,fsCom);    % Configura a frequ�ncia de amostragem
pause(0.01)
fwrite(dsPIC,31);

if tipo == 0
    coletando = true;
    contT=0;
    pause(0.5);
    fwrite(dsPIC,0);
    fprintf('\nRealizando coleta:   0/%3d ',dur);
    try
        fread(dsPIC,dsPIC.BytesAvailable);
    catch
    end
    drawnow
    pause(0.5);
    fwrite(dsPIC,35);   % Inicia a coleta
    while coletando
        %disp(dsPIC.BytesAvailable);
        fprintf('.');
        pause(0.1)
        fprintf('\b');
        pause(0.1)
    end
    
    % Reconstroi os dados
    dados = (dadosB(1:3:end))*256*256+(dadosB(2:3:end))*256+(dadosB(3:3:end));
    dados(dados>=8388608) = -(2^24-dados(dados>8388608));%8388608-dados(dados>8388608);
    dados = dados*3.3/8388608;
    
    % Reconstroi os canais
    dadosCol = zeros(nAmostra/16, 16);
    for i=1:16
        dadosCol(:,i) = dados(i:16:end);
    end
elseif tipo==1
    dadosCol = zeros(nAmostra, 16); 
    for canal=1:16
        dadosB = zeros(1,nAmostra*3);  % Prealoca a matriz para armazenar os dados brutos
        fprintf('\nRealizando coleta do canal %2d:   0/%3d ', canal-1, dur);
        pause(0.5)
        fwrite(dsPIC,canal-1);  % Altera o canal
        contT=0;
        coletando = true;
        pause(0.5)
        fwrite(dsPIC,34);       % Inicia a coleta
        while(coletando)        % Espera coleta terminar
            fprintf('.');
            pause(0.1)
            fprintf('\b');
            pause(0.1)
        end
        
        % Reconstroi os dados
        dados = (dadosB(1:3:end))*65536+(dadosB(2:3:end))*256+(dadosB(3:3:end));
        dados(dados>8388608) = 8388608-dados(dados>8388608);
        dados = dados*3.3/8388608;
        % Obt�m o controle
        controle = dadosB(4:4:end);
        %
        dadosCol(:, canal) = dados;
    end
    disp a
end


    function callback(~,~,~)
        inicio = contT*fs*3+1;
        contT = contT+1;
        fim = contT*fs*3;
        dadosB(inicio:fim) = fread(dsPIC, fs*3);
        fprintf('\b\b\b\b\b\b\b\b%3d/%3d ', contT, dur);
        
        if contT>=dur
            fwrite(dsPIC,254);
            pause(0.5);
            coletando = false;
            try
                fread(dsPIC, dsPIC.BytesAvailable);
            catch
            end
        end
    end
end
