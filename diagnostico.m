function diagnostico()
prompt = {'Frequ�ncia de Amostragem.:','Canal de Entrada:', 'Dura��o (s):',...
    'Tipo (0-Mux ou 1-Ind):', 'Frequ�ncia de Entrada'};
dlg_title = 'Diagn�stico';
num_lines = 1;
def = {'28000','0', '1', '0', '80'};
answer = inputdlg(prompt,dlg_title,num_lines,def);
if isempty(answer)
    disp('Diagn�stico cancelado!');
    return
end

fs = str2double(answer{1});
canalEnt=str2double(answer{2});
dur = str2double(answer{3});
tipo = str2double(answer{4});
porta = 'COM5';
freqEst = str2double(answer{5});
if tipo == 0
    Fs = fs/16;
else
    Fs = fs;
end
pause(0.5)
[ dadosCol] = coleta( fs, dur, tipo, porta ); % Coleta os dados
Y = fft(dadosCol)/Fs/dur;   % Calcula a fft
Y = abs(Y);                 % Obt�m o m�dulo
Y(1,:) = [];                % Remove o DC
Y(Fs*dur/2+1:end,:) = [];     % Remove as frequencias 'negativas'

energEnt = zeros(1,16);
energFund = zeros(1,16);
energ60 = zeros(1,16);
crosstalk = zeros(1,16);

% Sele��o dos bins a serem utilizados para cada c�lculo
binsFund = false(Fs/2,1);
bins60Hz = false(Fs/2,1);
binsEntr = false(Fs/2,1);
binsEntr(freqEst*dur+(-1:1)) = true;
bins60Hz(60*dur+(-1:1)) = true;
binsFund(10*dur:59*dur) = true;
if Fs/2>160
    binsFund(61*dur:160*dur) = true;
else
    binsFund(61*dur:end) = true;
end
binsFund(119*dur:121*dur)=false;
binsFund(bins60Hz) = false;
binsFund(binsEntr) = false;

for canal = 1:16
    energEnt(canal) = norm(Y(binsEntr, canal));    % Energia obtida na entrada
    energFund(canal) = norm(Y(binsFund, canal));   % Energia do ru�do de fundo
    energ60(canal) = norm(Y(bins60Hz, canal));     % Energia no 60Hz
end
for canal = 1:16
    crosstalk(canal) = energEnt(canal)/energEnt(canalEnt+1);
end
SNR = energEnt./energFund;
SNR60 = energ60./energFund;

Canal=0:15;
SNR = 20*log10(SNR);
SNR60 = 20*log10(SNR60);
crosstalk = 20*log10(crosstalk);

figure
cont=0;
for i=1:4
    for j=1:4
        cont=cont+1;
        subplot(4,4,cont);
        freq=(1:Fs*dur/2)/dur;
        plot(freq,Y(:,cont));
        xlim(freqEst+[-20,20]);
        title(['Canal ' num2str(cont-1)]);
    end
end

uisave({'dadosCol','crosstalk', 'SNR'},[answer{1} 'Hz, tipo ', answer{4}, ', ent ', answer{5} 'Hz, dur' answer{3} 's']);

T=table(Canal',crosstalk', SNR', SNR60','VariableNames',{'Canal' 'Crosstalk' 'SNR' 'SNR60'});
disp(T);
disp FIM!
end